import Hero from "./hero/Hero";
import Introduction from "./paragraphs/Introduction";

export default function Main() {
  return (
    <main>
      <Introduction />
    </main>
  );
}
