import ReactDOM from "react-dom/client";
import App from "./App";
import "./styles/main.css";

const target = document.getElementById("root");
if (!target) throw new Error("Failed to find target element");
const root = ReactDOM.createRoot(target);
root.render(<App />);
